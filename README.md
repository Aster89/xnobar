## xnobar, a marquee notification server for xmobar

XNobar is a notifications server plugin for [XMobar](https://codeberg.org/xmobar/xmobar) that shows notifications via an horizontally scrolling marquee.

![demo](https://codeberg.org/attachments/577e512a-bfb9-48ef-af04-21e915b9e36a)

## Disclaimer

It's still very much work in progress, but I'm using it everyday!

I'm not sure whether https://codeberg.org/Aster89/xnobar/issues/9 would really
obstruct the usage for others, so I'll try to address that soon.


## Thanks

I don't think I would have got such a project to this stage with the knowledge of Haskell I had before.

I learned a lot since when I started it, and I feel much more comfortable with Haskell, now.

But a lot of this learning was possible just by asking questions (on [StackOverflow](https://stackoverflow.com/users/5825294/enlico)), so many thanks go to those who answered [my Haskell questions lately](https://stackoverflow.com/search?tab=newest&q=user%3A5825294%20is%3Aquestion%20%5Bhaskell%5D&searchOn=3):

  - [K. A. Buhr](https://stackoverflow.com/users/7203016/k-a-buhr)
  - [duplode](https://stackoverflow.com/users/2751851/duplode)
  - [leftaroundabout](https://stackoverflow.com/users/745903/leftaroundabout)
  - [Daniel Wagner](https://stackoverflow.com/users/791604/daniel-wagner)
  - [chi](https://stackoverflow.com/users/3234959/chi)
  - [user1686](https://stackoverflow.com/users/49849/user1686)
  - [Noughtmare](https://stackoverflow.com/users/15207568/noughtmare)
  - [Mark Seemann](https://stackoverflow.com/users/126014/mark-seemann)
