module XNobar.Internal.Positive32 (Positive32, positive32, toWord32) where

import Data.Char (isDigit)
import Data.Word (Word32)
import Data.Semigroup (Max(Max))
import Text.ParserCombinators.ReadP

-- |Bare-bone non-zero version of [Data.Word.Word32](https://hackage.haskell.org/package/base-4.20.0.1/docs/Data-Word.html#t:Word32)
-- that implements the [Data.Semigroup.Max](https://hackage.haskell.org/package/base-4.19.1.0/docs/Data-Semigroup.html#t:Max)
-- typeclass.
newtype Positive32 = Positive32 { toWord32 :: Max Word32 } deriving (Eq, Ord, Semigroup, Show)

instance Monoid Positive32 where
  mempty = Positive32 1

instance Bounded Positive32 where
  minBound = Positive32 1
  maxBound = Positive32 (maxBound :: Max Word32)

instance Enum Positive32 where
  toEnum = error "toEnum is not meant to be used"
  fromEnum = error "fromEnum is not meant to be used"
  succ p@(Positive32 i)
    | p == maxBound = Positive32 1
    | otherwise = Positive32 $ succ i
  pred p@(Positive32 i) -- Not really meant to be used, but let's define it...
    | p == minBound = maxBound
    | otherwise = Positive32 $ pred i

-- |Smart ctor from [Data.Word.Word32](https://hackage.haskell.org/package/base-4.20.0.1/docs/Data-Word.html#t:Word32).
positive32 :: Word32 -> Positive32
positive32 0 = error "Attempt to create `Positive32` from `0`"
positive32 i = Positive32 $ Max i
