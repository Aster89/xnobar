-- |
-- = Notification server plugin for [XMobar](https://codeberg.org/xmobar/xmobar)
--
-- This module exposes the 'xNobar' smart value ctor to be used for
-- instantiating the plugin for [XMobar](https://codeberg.org/xmobar/xmobar),
-- together with the selectors of its fields to allow providing some options as
-- well.
--
-- That plugin will start a notification server, and show the notifications in
-- a [marquee](https://en.wikipedia.org/wiki/Marquee_element) as they come,
-- with a default text if there's no notification to show.
--
-- A single click on the marquee will dismiss the notification on which the click happened.
--
-- The present module can be thought of as the front-end part of the notifications server,
-- 'XNobar.Server' being the back-end.
{-# LANGUAGE LambdaCase #-}
module XNobar (xNobar, Config(..)) where

import Control.Monad (when)
import Xmobar (Exec (..))
import XNobar.Server (startServer)
import XNobar.Scroller (scroller, Config(..))

-- |XNobar plugin smart ctor. You would use it like this in your @xmobar.hs@
-- (__note__: the non-library usage of
-- [XMobar](https://codeberg.org/xmobar/xmobar) is not supported, or at least I
-- haven't tried using xnobar in that context):
--
-- @
-- main :: IO ()
-- main = xmobar defaultConfig {
--        , commands = [
--             Run $ xNobar { idleText = "All quiet here"
--                          , newsPrefix = "news: "
--                          , marqueeLength = 20
--                          , scrollPeriod = 1
--                          -- ...
--                          }
--        -- ...
-- @
xNobar = Config {
    idleText = "No notifications"
  , marqueeLength = 16
  , fontNumber = 0
  , scrollPeriod = 1
  , noNewsPrefix = ""
  , newsPrefix = "news: "
  , lineBreak = "\\n"
  , criticalPrefix =  " ! "
  , nonCriticalPrefix = ""
}

-- |This instance, required by [XMobar](https://codeberg.org/xmobar/xmobar),
-- is the core of the plugin.
instance Exec Config where
  alias _ = "XNobar"
  start config cb
    = startServer >>= \case Just notifs -> do scroller config cb notifs
                                              error "What? scroller returned. Has it been killed?!"
                            Nothing -> cb "<fc=#FF0000>Server could not start</fc>"
